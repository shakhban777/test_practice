const square = require('./square');

describe('Square number', () => {
  let mockValue;
  beforeEach(() => {
    // ADD TO BD
  })
  beforeAll(() => {
  })

  test('Correct value', () => {
    // expect(square(2)).toBe(4);
    // expect(square(2)).toBeLessThan(5);
    // expect(square(2)).toBeGreaterThan(3);
    // expect(square(2)).not.toBe(undefined);
    const spyMathPow = jest.spyOn(Math, 'pow');
    square(2);
    expect(spyMathPow).toBeCalledTimes(1);
  })

  test('Correct value', () => {
    const spyMathPow = jest.spyOn(Math, 'pow');
    square(1);
    expect(spyMathPow).toBeCalledTimes(0);
  })

  afterEach(() => {
    jest.clearAllMocks();
  })
  afterAll(() => {
  })
})
