const axios = require('axios');
const mapArrayToStrings = require('../mapArrayToStrings/mapArrayToStrings');

const getData = async () => {
  try {
    const response = await axios.get('https://jsonplaceholder.typicode.com/users');
    const userIds = response.data.map(user => user.id);
    return mapArrayToStrings(userIds);
  } catch (e) {
    console.error(e);
  }
}

module.exports = getData;
