const validateValue = require('./validateValue');

test('Validate value', () => {
  expect(validateValue(50)).toBe(true);
})

describe('Validate value', () => {
  test('Correct value', () => {
    expect(validateValue(50)).toBe(true);
  })
  test('Less than correct value', () => {
    expect(validateValue(-1)).toBe(false);
  })
  test('More than correct value', () => {
    expect(validateValue(101)).toBe(false);
  })
  test('Border value', () => {
    expect(validateValue(0)).toBe(true);
  })
  test('Border value', () => {
    expect(validateValue(100)).toBe(true);
  })
})
