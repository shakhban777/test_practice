const mapArrayToStrings = require('./mapArrayToStrings');

describe('Map array to strings', () => {
  test('Correct value', () => {
    expect(mapArrayToStrings([1, 2, 3])).toEqual(['1', '2', '3']);
  })
  test('Blended values', () => {
    expect(mapArrayToStrings([1, 2, 3, null, undefined, 'test'])).toEqual(['1', '2', '3']);
  })
  test('Empty array', () => {
    expect(mapArrayToStrings([])).toEqual([]);
  })
  test('Negative', () => {
    expect(mapArrayToStrings([1, 2, 3])).not.toEqual([1, 2, 3, 4]);
  })
})
